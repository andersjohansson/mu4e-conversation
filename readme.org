#+TITLE: mu4e-conversation

This package offers an alternate view to [[http://www.djcbsoftware.nl/code/mu/][mu4e]] e-mail display.  It shows all
e-mails of a thread in a single view, where each correspondant has their own
face.  Threads can be displayed linearly (in which case e-mails are displayed
in chronological order) or as an Org document where the node tree maps the
thread tree.

* Installation

Get the package, either from MELPA:

: M-x package-install RET mu4e-conversation RET

Or clone / download this repository and modify your ~load-path~.

: (add-to-list 'load-path (expand-file-name "/path/to/mu4e-conversation/" user-emacs-directory))

Load the package with

: (with-eval-after-load 'mu4e (require 'mu4e-conversation))

From the headers view, call ~M-x mu4e-conversation~.

To fully replace ~mu4e-view~ with ~mu4e-conversation~ from any other command
(e.g. ~mu4e-headers-next~, ~helm-mu~), call

: (setq mu4e-view-func 'mu4e-conversation)

* Screenshots

  #+ATTR_HTML: :width 800px
  [[./mu4e-conversation-linear.png]]

  #+ATTR_HTML: :width 800px
  [[./mu4e-conversation-tree.png]]
